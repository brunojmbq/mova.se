import { Mova.SePage } from './app.po';

describe('mova.se App', () => {
  let page: Mova.SePage;

  beforeEach(() => {
    page = new Mova.SePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
