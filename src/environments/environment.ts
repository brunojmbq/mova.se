// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDayEI927o0KHrBfjKXoNp2SWnnXUgoS80',
    databaseURL: 'https://movase-6d2f2.firebaseio.com',
    storageBucket: 'gs://movase-6d2f2.appspot.com',
    projectId: 'movase-6d2f2',
  }
};
