export class Event {
    public nome: string;
    public esporte: string;
    public local: string;
    public date: Date;
    public data: number;
    public distancia = 0.8;
}