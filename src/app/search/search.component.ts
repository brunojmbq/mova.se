import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FirebaseListObservable } from 'angularfire2/database';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { EventService } from './../services/event.service';

@Component({
    moduleId: module.id,
    selector: 'search',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.css']
})
export class SearchComponent {

    eventos: FirebaseListObservable<any>;
    searching: boolean = false;
    eventsCtrl: FormControl;
    filteredEvents: any;

    constructor(eventService: EventService) {
        this.eventos = eventService.listAll();
        this.eventsCtrl = new FormControl();
        this.filteredEvents = this.eventsCtrl.valueChanges
            .startWith(null)
            .switchMap(name => this.filterEvents(name));
    }

     filterEvents(val: string) {
         return val ? this.eventos.map(list => list.filter(
             s => new RegExp(`^${val}`, 'gi').test(s.nome)))
              : this.eventos;
      }

    onClickSearch() {
        this.searching = !this.searching;
    }

}
