import { EventService } from './services/event.service';
import { FirebaseListObservable } from 'angularfire2/database';
import { Component } from '@angular/core';
import { TdMediaService } from '@covalent/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

    eventos: FirebaseListObservable<any>;
    filteredEvents: any;

    constructor(private eventService: EventService, public media: TdMediaService){
       this.eventos = eventService.listAll();
    }

    onSearch(term):void {
      term ? this.filteredEvents = this.eventos.map(list => list.filter(
             s => new RegExp(`^${term}`, 'gi').test(s.nome)))
              : this.eventos;
    }

}