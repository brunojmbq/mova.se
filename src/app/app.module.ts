import { SportService } from './services/sport.service';
import { EventService } from './services/event.service';
import { AppRoutingModule } from './app-routing.module';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { CovalentLayoutModule, CovalentSearchModule, CovalentMediaModule } from '@covalent/core';
import { MaterialModule } from './material.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewEventComponent, NewEventDialogComponent } from './new-event/new-event.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NewEventComponent,
    NewEventDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CovalentSearchModule,
    CovalentLayoutModule,
    CovalentMediaModule
  ],
  entryComponents: [NewEventDialogComponent],
  providers: [EventService, SportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
