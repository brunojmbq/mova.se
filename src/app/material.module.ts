import { NgModule } from '@angular/core';

import { MdIconModule } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { MdCardModule } from '@angular/material';
import { MdGridListModule } from '@angular/material';
import { MdInputModule } from '@angular/material';
import { MdSelectModule } from '@angular/material';
import { MdDatepickerModule } from '@angular/material';
import { MdNativeDateModule } from '@angular/material';
import { MdAutocompleteModule } from '@angular/material';
import { MdDialogModule } from '@angular/material';

@NgModule({
    imports: [
        MdIconModule,
        MdButtonModule,
        MdCardModule,
        MdGridListModule,
        MdInputModule,
        MdSelectModule,
        MdDatepickerModule,
        MdNativeDateModule,
        MdAutocompleteModule,
        MdDialogModule
    ],
    exports: [
        MdIconModule,
        MdButtonModule,
        MdCardModule,
        MdGridListModule,
        MdInputModule,
        MdSelectModule,
        MdDatepickerModule,
        MdNativeDateModule,
        MdAutocompleteModule,
        MdDialogModule
    ]
})
export class MaterialModule {

}
