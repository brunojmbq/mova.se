import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
 
import { DashboardComponent }   from './dashboard/dashboard.component';
import { NewEventComponent } from './new-event/new-event.component';
 
const appRoutes: Routes = [
  { path: '',   redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'new-event', component: NewEventComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }