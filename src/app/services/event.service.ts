import { Event } from './../event';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class EventService {

    constructor(private db: AngularFireDatabase) { }

    listNearest(): FirebaseListObservable<Event[]> {
        return this.db.list('/eventos', {
            query: {
                orderByChild: 'distancia'
            }
        });
    }

    listAll(): FirebaseListObservable<Event[]> {
        return this.db.list('/eventos');
    }

    save(event: Event): void {
        this.db.database.ref().child("eventos").push(event);
    }
}