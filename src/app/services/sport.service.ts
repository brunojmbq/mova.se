import { FirebaseListObservable, AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class SportService {

  constructor(private db: AngularFireDatabase) { }

  listAll(): FirebaseObjectObservable<any> {
        return this.db.object('/esportes');
    }
}