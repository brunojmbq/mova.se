import { Component } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { EventService } from './../services/event.service';

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.css']
})
export class DashboardComponent {
  eventos: FirebaseListObservable<any>;

  constructor(eventService: EventService) {
    this.eventos = eventService.listNearest();
  }
 
}
