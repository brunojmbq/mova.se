import { FormControl } from '@angular/forms';
import { SportService } from './../services/sport.service';
import { EventService } from './../services/event.service';
import { Component } from '@angular/core';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { MdDialog, MdDialogRef } from '@angular/material';

import { Event } from './../event';

@Component({
    moduleId: module.id,
    selector: 'new-event',
    templateUrl: 'new-event.component.html',
    styleUrls: ['new-event.component.css']
})
export class NewEventComponent {
    esportes: FirebaseObjectObservable<any>;
    evento = new Event();
    today = new Date();

    constructor(
        private eventService: EventService,
        private sportService: SportService,
        private dialog: MdDialog) {
        this.esportes = sportService.listAll();
        this.evento.esporte = null;
    }

    onSelectDate(): void {
        alert('teste');
    }

    onSubmit(eventForm: FormControl) {
        if (eventForm.valid) {
            this.evento.data = this.evento.date.getTime();
            this.eventService.save(this.evento);
            this.dialog.open(NewEventDialogComponent);
        }
    }
}

@Component({
    selector: 'new-event-dialog',
    template: `
        <h2 md-dialog-title>Sucesso</h2>
        <md-dialog-content>Evento criado com sucesso!</md-dialog-content>
        <md-dialog-actions>
            <button md-button md-dialog-close routerLink="/dashboard">OK</button>
        </md-dialog-actions>
    `,
})
export class NewEventDialogComponent {

    public title: string;
    public message: string;

    constructor(public dialogRef: MdDialogRef<NewEventDialogComponent>) { }
}
